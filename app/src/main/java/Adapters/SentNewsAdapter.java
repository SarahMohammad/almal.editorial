package Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.ikdynamics.elmalnewseditorial.MainActivity;
import com.ikdynamics.elmalnewseditorial.R;
import java.util.List;
import Helpers.Constants;
import Helpers.ServiceHandler;
import Models.Response;
import Models.TopicsModel;


public class SentNewsAdapter extends
        RecyclerView.Adapter<SentNewsAdapter.ViewHolder> {


    private static Context context;
    private final List<TopicsModel> topics;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;


    public SentNewsAdapter(List<TopicsModel> topics, Context context) {
        this.topics = topics;
        SentNewsAdapter.context = context;

    }

    @Override
    public SentNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView;
        ViewHolder viewHolder;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cardview_news_row, null);
        viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SentNewsAdapter.ViewHolder holder, final int position) {

//        notifyDataSetChanged ();
//        holder.newsDate.setText(topics.get(position).getArticle_Section_ID()+"");
        holder.newsTitle.setText(topics.get(position).getArticle_Title());

        if(topics.get(position).getArticle_ImgeBytes()==null){
            Log.e("tag", "no pic inserted");
        }else {
            holder.newsImg.setImageBitmap(Constants.decodeBase64(topics.get(position).getArticle_ImgeBytes()));
        }


        holder.sendNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the object i want to send now and delete from shared
                Gson gson = new Gson();
                Log.e("topics size", topics.size() + "");
                //the object to be sent to webservice
                sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_APPEND);
                editor = sharedPref.edit();

                editor.putString("objectToSend", gson.toJson(topics.get(position)));
                editor.putInt("position", position);
                Log.e(topics.size()+"",position+"");
                editor.apply();
                new sendData().execute(Constants.url + "AddArticle");

            }
        });
    }
//remove item from recycler and update the list in shared

    /**
     * removeItemAt method deletes an item from list of sent news when it is sent to server
     * @param position of item in the recycler view to be removed
     */
    private void removeItemAt(int position) {
        Gson gson = new Gson();
        topics.remove(position);
        Log.e("new topics size", topics.size() + "");
        editor.putString("sentList", gson.toJson(topics));
        editor.apply();
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView newsTitle;
        final ImageView newsImg;
        final ImageView sendNowBtn;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            newsTitle = (TextView) itemLayoutView.findViewById(R.id.news_title);
            newsImg =(ImageView) itemLayoutView.findViewById(R.id.news_image);
            sendNowBtn = (ImageView) itemLayoutView.findViewById(R.id.send_now_button);
            sendNowBtn.setVisibility(View.VISIBLE);
        }
    }


    //post object to server async task
    public class sendData extends AsyncTask<String, Void, Response> {
        String object;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showProgressDialog(context);
        }

        @Override
        protected Response doInBackground(String... params) {

            Response result ;
            Gson gson = new Gson();
            ServiceHandler sh = new ServiceHandler();
            sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_APPEND);

            object = sharedPref.getString("objectToSend", null);
            Log.e("o", gson.toJson(object));
            String response = sh.makeServiceCall(params[0],object);
            int status = sh.status;
            if (status == 200 && response != null) {
                result = gson.fromJson(response, Response.class);
                Log.e("user", response);

                return result;
            }
            else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            Constants.hideProgressDialog();


            if (result != null && result.getResult().equals("Success")) {
                Log.e("s", "success");
                int pos = sharedPref.getInt("position", -1);
                removeItemAt(pos);
                MainActivity.fireUi();

            }else if(result!=null && result.getResult().equals("Fail-Exist-Story")){
                Toast.makeText(context,R.string.already_exists,Toast.LENGTH_SHORT).show();
                int pos = sharedPref.getInt("position", -1);
                removeItemAt(pos);
                MainActivity.fireUi();

            }else{
                Log.e("result",result+"");
                Toast.makeText(context,R.string.failed,Toast.LENGTH_SHORT).show();
            }
        }
    }
}
