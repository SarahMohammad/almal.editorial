package Helpers;



import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServiceHandler {

//    public final static int GET = 1;
//    public final static int POST = 2;
    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public  int status;

    public ServiceHandler() {

    }

    private final OkHttpClient client = new OkHttpClient();



    //Get Method
    public String makeServiceCall(String url) {

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-type","application/json")
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            status= response.code();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Post Method
    public String makeServiceCall(String url, String json) {


        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            status= response.code();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}