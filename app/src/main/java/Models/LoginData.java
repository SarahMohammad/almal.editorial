package Models;

public class LoginData {

    private String  ROLID;
    private String  USERID;
    private String  UserName;

    public String getROLID() {
        return ROLID;
    }

    public String getUSERID() {
        return USERID;
    }

    public String getUserName() {
        return UserName;
    }

}
