package Models;

public class TopicsModel {


     String Article_AddedBy;
     String Article_AdderRuleID;
    private String Article_Body;
    private String Article_ImgeBytes;
    private int Article_Section_ID;
    private String  Article_Section_Name;
    private String Article_Title;
    private  String Article_ImgeTitle;


    public String getArticle_ImgeTitle() {
        return Article_ImgeTitle;
    }

    public void setArticle_ImgeTitle(String article_ImgeTitle) {
        Article_ImgeTitle = article_ImgeTitle;
    }

    public String getArticle_Section_Name() {
        return Article_Section_Name;
    }

    public void setArticle_Section_Name(String article_Section_Name) {
        Article_Section_Name = article_Section_Name;
    }

    public String getArticle_Title() {
        return Article_Title;
    }

    public void setArticle_Title(String article_Title) {
        Article_Title = article_Title;
    }

    public int getArticle_Section_ID() {
        return Article_Section_ID;
    }

    public void setArticle_Section_ID(int article_Section_ID) {
        Article_Section_ID = article_Section_ID;
    }





    public String getArticle_ImgeBytes() {
        return Article_ImgeBytes;
    }

    public void setArticle_ImgeBytes(String article_ImgeBytes) {
        Article_ImgeBytes = article_ImgeBytes;
    }

    public String getArticle_Body() {
        return Article_Body;
    }

    public void setArticle_Body(String article_Body) {
        Article_Body = article_Body;
    }


    public void setArticle_AdderRuleID(String article_AdderRuleID) {
        Article_AdderRuleID = article_AdderRuleID;
    }



    public void setArticle_AddedBy(String article_AddedBy) {
        Article_AddedBy = article_AddedBy;
    }
}
