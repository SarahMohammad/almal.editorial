package com.ikdynamics.elmalnewseditorial;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import Adapters.RadioListAdapter;
import Helpers.ConnectionDetector;
import Helpers.Constants;
import Helpers.ServiceHandler;
import Models.SectionsModel;
import Models.TopicsModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddNewsActivity extends AppCompatActivity {


    private TextView addedSection;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private Dialog  pic_dialog, sectionsDialog;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private  Gson gson;
    private TopicsModel topics, sentTopics;
    private ArrayList<TopicsModel> topicList, sentTopicsList;

    private Bitmap photo;

    private String imgString;
    private static  final int REQUEST_CAMERA = 1;
    private static  final int REQUEST_OTHER = 2;
    private static  final int REQUEST_CODE_ASK_CAMERA_PERMISSIONS = 0;
    private static  final int REQUEST_CODE_ASK_STORAGE_PERMISSIONS = 3;
    private String roleID,userID;
    private int position;
    private int objectSectionID;
    private String objectSectionName;
    private int sec;
    private String secN;
    private ProgressBar progress;
    private ImageView addImageBtn,discard,changePic;
    private EditText title,content;
    private LinearLayout addDept;
    private RelativeLayout   changePicLayout ;
    /**
     * attachBaseContext method to modify the font of the application to FrutigerLTArabic-45Light
     * @param newBase  application context
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private void findViews(){
        progress = (ProgressBar) findViewById(R.id.progressBar);
        addImageBtn = (ImageView) findViewById(R.id.add_image_btn);
        addedSection = (TextView) findViewById(R.id.added_section);
        title = (EditText) findViewById(R.id.title);
        content = (EditText) findViewById(R.id.content);
        discard = (ImageView) findViewById(R.id.discard_add_news_icon);
        changePic = (ImageView) findViewById(R.id.change_picture);
        addDept=(LinearLayout) findViewById(R.id.add_dept_layout);
        changePicLayout =(RelativeLayout) findViewById(R.id.change_picture_layout);
    }

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        assert toolbar!=null;
        setSupportActionBar(toolbar);
        findViews();
        title.setBackgroundResource(R.color.light_grey);

        ImageView send = (ImageView) toolbar.findViewById(R.id.send_btn);
        ImageView save = (ImageView)toolbar.findViewById(R.id.save_btn);
        topicList = new ArrayList<>();
        sharedPref = getSharedPreferences("myPrefs",Context.MODE_APPEND);
        editor = sharedPref.edit();
        gson = new Gson();
        objectSectionID = -1;
        objectSectionName =null;
        roleID = sharedPref.getString("roleID",null);
        userID = sharedPref.getString("userID",null);
            assert getSupportActionBar() != null;
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
            savedListExists();
            sentListExists();

            //X button
            discard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
            }
         });

        //change picture
        changePicLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePictureDialog();
            }
        });


        //sent button
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPref.getInt("sec", 0) != 0) {
                    sec = sharedPref.getInt("sec", 0);
                    secN = sharedPref.getString("sectionName", null);
                }
                if (sec == 0 || title.getText().toString().equals("") || content.getText().toString().equals("")) {
                    Toast.makeText(AddNewsActivity.this, R.string.check_input, Toast.LENGTH_SHORT).show();
                } else {
                    topics = new TopicsModel();

                    if (getIntent().hasExtra("openFirstSavedCard")) {
                        topicList.remove(0);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }

                    if (getIntent().hasExtra("openSecondSavedCard")) {
                        topicList.remove(1);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }

                    if (getIntent().hasExtra("newsToUpdate")) {
                        Gson gson = new Gson();
                        topicList.remove(position);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }

                        sentTopics = new TopicsModel();
                        sentTopics.setArticle_Title(title.getText().toString());
                        sentTopics.setArticle_Body(content.getText().toString());
                        sentTopics.setArticle_ImgeBytes(imgString);
                        sentTopics.setArticle_AddedBy(roleID);
                        sentTopics.setArticle_AdderRuleID(userID);
                        sentTopics.setArticle_Section_ID(sec);
                    sentTopics.setArticle_ImgeTitle("");
                        sentTopics.setArticle_Section_Name(secN);
                        editor.apply();
                        Log.e("send_topics_", gson.toJson(sentTopics) + "");
                        sentTopicsList.add(sentTopics);
                        editor.putString("sentList", gson.toJson(sentTopicsList));
                    editor.putString("sectionName", null);
                    editor.putInt("sec", 0);
                    editor.apply();
                        editor.apply();
                        startService(new Intent(AddNewsActivity.this, MyService.class));
                        Intent intent = new Intent(AddNewsActivity.this, MainActivity.class);
                        intent.putExtra("key", "sent");
                        startActivity(intent);


                }
            }
        });


        // save button
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if there is data in shared means i am updating an existing topic
                if (sharedPref.getInt("sec", 0) != 0) {
                    sec = sharedPref.getInt("sec", 0);
                    secN = sharedPref.getString("sectionName", null);
                }
                if (sec == 0 || title.getText().toString().equals("") || content.getText().toString().equals("")) {
                    Toast.makeText(AddNewsActivity.this, R.string.check_input, Toast.LENGTH_SHORT).show();
                } else {
                    topics = new TopicsModel();
                    if (getIntent().hasExtra("openFirstSavedCard")) {
                        topicList.remove(0);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }
                    if (getIntent().hasExtra("openSecondSavedCard")) {
                        topicList.remove(1);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }
                    if (getIntent().hasExtra("newsToUpdate")) {
                        Gson gson = new Gson();
                        topicList.remove(position);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.apply();
                    }
                        topics.setArticle_Title(title.getText().toString());
                        topics.setArticle_Body(content.getText().toString());
                        topics.setArticle_ImgeBytes(imgString);
                        topics.setArticle_AddedBy(roleID);
                        topics.setArticle_AdderRuleID(userID);
                        topics.setArticle_Section_ID(sec);
                        topics.setArticle_ImgeTitle("");
                        topics.setArticle_Section_Name(secN);
                        Log.e("topics_", gson.toJson(topics) + "");
                        topicList.add(topics);
                        editor.putString("savedList", gson.toJson(topicList));
                        editor.putString("sectionName", null);
                        editor.putInt("sec", 0);
                        editor.apply();
                        Intent intent = new Intent(AddNewsActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                }
            }
        });

        //opening section dialog
        addDept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sectionsDialog = new Dialog(AddNewsActivity.this,R.style.PauseDialog);
                sectionsDialog.setContentView(R.layout.sections_dialog_prompt);
                sectionsDialog.setCancelable(true);

                sectionsDialog.setTitle(R.string.choose_section);

                mRecyclerView = (RecyclerView) sectionsDialog.findViewById(R.id.sections_recycler_view);
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(AddNewsActivity.this);
                mRecyclerView.setLayoutManager(mLayoutManager);
                String secList = sharedPref.getString("sectionsList", null);
                ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
                if (connectionDetector.isConnectingToInternet()) {
                    //connected to internet call the sections webservice
                    new GetSections(objectSectionID,objectSectionName).execute(Constants.url+"GetAlmalSections");
                    Log.e("TAG","webservice called");
                }
                //not connected get sections from shared
                else if(secList!=null&&!secList.isEmpty()){
                    Log.e("TAG","get the sections from shared");
                    ArrayList<SectionsModel>sec= new ArrayList<>(Arrays.asList(gson.fromJson(secList, SectionsModel[].class)));
                    mAdapter = new RadioListAdapter(AddNewsActivity.this, sec, sectionsDialog,addedSection,objectSectionID,objectSectionName);
                    mRecyclerView.setAdapter(mAdapter);
                    sectionsDialog.show();
                }
            }
        });

        }//onCreate end

    /**
     * savedListExists to check if the shared has a savedList and whether the user opened
     * details from main activity or from the saved list recycler view then sets the ui with
     * the selected object data
     */
    private void savedListExists(){
        //for first time
        if(sharedPref.contains("savedList")){
            Log.e("tag", "contains data");

            String savedList = sharedPref.getString("savedList", null);
            topicList = new ArrayList<>(Arrays.asList(gson.fromJson(savedList, TopicsModel[].class)));

            //if i am opening the saved card from main activity to view details
            if(getIntent().hasExtra("openFirstSavedCard")){
                TopicsModel fTopic = topicList.get(0);
                title.setText(fTopic.getArticle_Title());
                content.setText(fTopic.getArticle_Body());
                objectSectionID = fTopic.getArticle_Section_ID();
                objectSectionName = fTopic.getArticle_Section_Name();
                sec = objectSectionID;
                secN = objectSectionName;
                addedSection.setText(objectSectionName);
                if(fTopic.getArticle_ImgeBytes()!=null) {
                    changePic.setImageBitmap(Constants.decodeBase64(fTopic.getArticle_ImgeBytes()));
                }
                imgString = fTopic.getArticle_ImgeBytes();
            }

            if(getIntent().hasExtra("openSecondSavedCard")){
                TopicsModel fTopic = topicList.get(1);
                title.setText(fTopic.getArticle_Title());
                content.setText(fTopic.getArticle_Body());
                objectSectionID = fTopic.getArticle_Section_ID();
                objectSectionName = fTopic.getArticle_Section_Name();
                addedSection.setText(objectSectionName);
                sec = objectSectionID;
                secN = objectSectionName;
                editor.apply();

                if(fTopic.getArticle_ImgeBytes()!=null) {
                    changePic.setImageBitmap(Constants.decodeBase64(fTopic.getArticle_ImgeBytes()));
                }
                imgString = fTopic.getArticle_ImgeBytes();

            }

            //when navigating from saved adapter to update a saved news
            // check if the intent has the object to be changed
            if(getIntent().hasExtra("newsToUpdate")){
                position = getIntent().getIntExtra("newsToUpdatePosition", -1);
                Log.e("Position", position + "");
                TopicsModel topic= topicList.get(position);
                title.setText(topic.getArticle_Title());
                content.setText(topic.getArticle_Body());
                objectSectionID = topic.getArticle_Section_ID();
                objectSectionName = topic.getArticle_Section_Name();
                sec = objectSectionID;
                secN = objectSectionName;
                addedSection.setText(objectSectionName);
                Log.e("me", topic.getArticle_Section_ID()+"");
                Log.e("topics",gson.toJson(topic)+"");
                editor.apply();
                if(topic.getArticle_ImgeBytes()!=null) {
                    changePic.setImageBitmap(Constants.decodeBase64(topic.getArticle_ImgeBytes()));
                }
                imgString = topic.getArticle_ImgeBytes();
            }
        }else{
            topicList = new ArrayList<>();
        }
    }

    /**
     * sentListExists to check if the shared has a sentList
     * and get it
     */
    private void sentListExists(){
        if(sharedPref.contains("sentList")){
            Log.e("tag","contains data");
            String sentList = sharedPref.getString("sentList", null);
            sentTopicsList = new ArrayList<>(Arrays.asList(gson.fromJson(sentList, TopicsModel[].class)));
        }else{
            sentTopicsList = new ArrayList<>();
        }
        if(changePic.getDrawable()!=null){
            addImageBtn.setVisibility(View.GONE);

        }else{
            addImageBtn.setVisibility(View.VISIBLE);

        }

    }

    /**
     * openCameraIntent function to open the camera when the user chooses camera and dismisses the
     * invoking dialog
     */
    private void openCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
        pic_dialog.dismiss();
    }

    /**
     * openGalleryIntent function to open the phone gallery when the user chooses gallery and dismisses the
     * invoking dialog
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_OTHER);
        pic_dialog.dismiss();
    }

    /**
     * changePictureDialog function to initiate the picture dialog and
     * either openCameraIntent or openGalleryIntent upon user choice
     */
    private void changePictureDialog() {
        pic_dialog = new Dialog(AddNewsActivity.this);
        pic_dialog.setContentView(R.layout.change_userpicture_prompt);
        ImageView camera = (ImageView) pic_dialog.findViewById(R.id.layout_camera);
        ImageView phoneGallery = (ImageView) pic_dialog.findViewById(R.id.layout_phone);

        pic_dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasCameraPermission = AddNewsActivity.this.checkSelfPermission(Manifest.permission.CAMERA);
                    if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CODE_ASK_CAMERA_PERMISSIONS);
                        return;
                    }
                }
                openCameraIntent();
            }
        });

        phoneGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasStoragePermission = 0;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        hasStoragePermission = AddNewsActivity.this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                    if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQUEST_CODE_ASK_STORAGE_PERMISSIONS);
                        }
                        return;
                    }
                }
                openGalleryIntent();
            }
        });
    }

    /**
     * onRequestPermissionsResult method is invoked for every call on requestPermissions
     *in changePictureDialog function
     * @param requestCode The request code passed
     * @param permissions The requested permissions
     * @param grantResults The grant results for the corresponding permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_CAMERA_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    openCameraIntent();
                } else {
                    // Permission Denied
                    Toast.makeText(AddNewsActivity.this,R.string.ACCESS_CAMERA_DENIED, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case REQUEST_CODE_ASK_STORAGE_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    openGalleryIntent();
                } else {
                    // Permission Denied
                    Toast.makeText(AddNewsActivity.this,R.string.ACCESS_Gallary_DENIED, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("requestCode", requestCode + "");

        if (data != null) {

            if (resultCode !=RESULT_CANCELED) {
                addImageBtn.setVisibility(View.GONE);
                if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
                    photo = (Bitmap) data.getExtras().get("data");
                } else if (requestCode == REQUEST_OTHER) {
                    try {
                       Uri selectedImageUri = data.getData();
                        Log.e("selectedImageUri", selectedImageUri+"");
                        photo = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                imgString = Base64.encodeToString(Constants.getBytesFromBitmap(photo), Base64.NO_WRAP);
                changePic.setImageBitmap(Constants.decodeBase64(imgString));
                Log.e("64", imgString+"");


            }
        } else {
            Toast.makeText(AddNewsActivity.this.getApplicationContext(), R.string.error, Toast.LENGTH_LONG).show();
        }
    }

    //get all sections from server
    public class GetSections extends AsyncTask<String, Void, SectionsModel[]> {

        final int sectionId;
        final String sectionName;


        public GetSections(int section_ID, String section_name) {
            this.sectionId =section_ID;
            this.sectionName = section_name;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected SectionsModel[] doInBackground(String... params) {

            SectionsModel[] sections;
            Gson gson = new Gson();
            ServiceHandler sh = new ServiceHandler();
            String response = sh.makeServiceCall(params[0]);
            int status = sh.status;
            if (status == 200 && response != null) {
                sections = gson.fromJson(response, SectionsModel[].class);
                Log.e("Sections", response);
                return  sections;

            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(SectionsModel[] sections) {
            super.onPostExecute(sections);
            progress.setVisibility(View.INVISIBLE);

            if (sections != null) {
                ArrayList<SectionsModel> sectionList = new ArrayList<>(Arrays.asList(sections));
                Log.e("tag", new Gson().toJson(sections) + "");

                editor = sharedPref.edit();
                editor.putString("sectionsList",gson.toJson(sectionList));
                editor.apply();

                mAdapter = new RadioListAdapter(AddNewsActivity.this, sectionList, sectionsDialog,addedSection,sectionId, sectionName);
                mRecyclerView.setAdapter(mAdapter);
                sectionsDialog.show();
            } else {
                if (sharedPref.contains("sectionsList")) {
                    Log.e("tag", "server error but got the data from shared");
                    String sentList = sharedPref.getString("sectionsList", null);
                    ArrayList<SectionsModel>sModelList = new ArrayList<>(Arrays.asList(gson.fromJson(sentList, SectionsModel[].class)));
                    mAdapter = new RadioListAdapter(AddNewsActivity.this, sModelList, sectionsDialog,addedSection,sectionId, sectionName);
                    mRecyclerView.setAdapter(mAdapter);
                    sectionsDialog.show();

                } else {
                    Log.e("tag", "server error");
                }
            }
        }
    }
}