package com.ikdynamics.elmalnewseditorial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import Adapters.SavedNewsAdapter;
import Helpers.Constants;
import Models.TopicsModel;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AllSavedNews extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_news);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_saved);
        assert   toolbar!=null;
        setSupportActionBar(toolbar);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.saved_news_recycler_view);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow ;
        upArrow = ResourcesCompat.getDrawable(getResources(),R.drawable.back_arrow,null);
        assert upArrow != null;
        upArrow.setColorFilter(ContextCompat.getColor(AllSavedNews.this, R.color.light_grey), PorterDuff.Mode.SRC_ATOP);

        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        Gson gson = new Gson();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        Constants.changeStatusBarColor(getWindow(), AllSavedNews.this);
        SharedPreferences sharedPref = getSharedPreferences("myPrefs",Context.MODE_APPEND);
        String savedList = sharedPref.getString("savedList", null);

        if(savedList==null){
            Log.e("saved","saved = null");
        }else {
            ArrayList<TopicsModel> list = new ArrayList<>(Arrays.asList(gson.fromJson(savedList, TopicsModel[].class)));
            assert mRecyclerView!=null;
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(AllSavedNews.this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            RecyclerView.Adapter mAdapter = new SavedNewsAdapter(list, AllSavedNews.this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(upIntent)
                        .startActivities();
            } else {
                NavUtils.navigateUpTo(this, upIntent);
            }
            return true;
        }
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }
}