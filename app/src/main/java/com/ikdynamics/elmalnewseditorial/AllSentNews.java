package com.ikdynamics.elmalnewseditorial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import Adapters.SentNewsAdapter;
import Models.TopicsModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AllSentNews extends AppCompatActivity {

    static  RecyclerView mRecyclerView;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_sent);
        assert toolbar!=null;
        setSupportActionBar(toolbar);
        assert  getSupportActionBar()!=null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ResourcesCompat.getDrawable(getResources(),R.drawable.back_arrow,null);
        assert upArrow != null;
        upArrow.setColorFilter(ContextCompat.getColor(AllSentNews.this,R.color.light_grey), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

         mRecyclerView = (RecyclerView) findViewById(R.id.sent_news_recycler_view);
        Gson gson = new Gson();
        SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_APPEND);
        String sentList = sharedPref.getString("sentList", null);
        if(sentList==null){
            Log.e("sent", "sent = null");
//            Toast.makeText(AllSentNews.this,"sent = null",Toast.LENGTH_SHORT).show();
        }else{
            ArrayList<TopicsModel> list = new ArrayList<>(Arrays.asList(gson.fromJson(sentList, TopicsModel[].class)));
            assert  mRecyclerView!=null;
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(AllSentNews.this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            RecyclerView.Adapter mAdapter = new SentNewsAdapter(list, AllSentNews.this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
//    public static void hideRecycler(){
//        mRecyclerView.setVisibility(View.INVISIBLE);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(upIntent)
                        .startActivities();
            } else {
                NavUtils.navigateUpTo(this, upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
