package com.ikdynamics.elmalnewseditorial;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import Helpers.ConnectionDetector;
import Helpers.Constants;
import Helpers.ServiceHandler;
import Models.LoginData;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class LoginActivity extends AppCompatActivity {


    private EditText userName;
    private EditText password;
    private Intent intent;
    private SharedPreferences sharedPref;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Intent thisIntent = LoginActivity.this.getIntent();
        startActivity(thisIntent);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        userName  = (EditText) findViewById(R.id.user_name_edittext);
        Button buttonLogin = (Button) findViewById(R.id.login_button);
        password = (EditText) findViewById(R.id.password_edittext);

        Constants.changeStatusBarColor(getWindow(), LoginActivity.this);
        sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);

        int checkLogin = sharedPref.getInt("firstLogin",0);
        if (checkLogin == 1){
            intent = new Intent(getApplication(), MainActivity.class);
            startActivity(intent);
            finish();
        }
        assert buttonLogin!=null;
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();
                name = name.trim();
                name = name.replaceAll("\\s+", "");
                String pass = password.getText().toString();
                pass = pass.trim();
                pass = pass.replaceAll("\\s+", "");
                if (pass.length() < 1 || name.length() < 1) {
                    Toast.makeText(getBaseContext(), R.string.check_input, Toast.LENGTH_SHORT).show();
                } else {
                    ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
                    if (connectionDetector.isConnectingToInternet()) {
                        new LoginService().execute(Constants.url + "GetEditorialUser/" + name + "/" + pass);
                    } else {
                        Toast.makeText(LoginActivity.this, R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }


    private class LoginService extends AsyncTask<String, Void, LoginData> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showProgressDialog(LoginActivity.this);
        }
        @Override
        protected LoginData doInBackground(String... params) {
            LoginData user;
            Gson gson = new Gson();
            ServiceHandler sh = new ServiceHandler();
            String response = sh.makeServiceCall(params[0]);
            int status = sh.status;
            if (status == 200 && response != null) {
                user = gson.fromJson(response, LoginData.class);
                Log.e("user", response);
            } else {
                return null;
            }
            return user;
        }

        @Override
        protected void onPostExecute(LoginData user) {
            super.onPostExecute(user);

            Constants.hideProgressDialog();

            if (user != null&& !user.getUSERID().isEmpty() && !user.getROLID().isEmpty()&& !user.getUserName().isEmpty()) {
                if(sharedPref.contains("userName")) {
                    String j = sharedPref.getString("userName", null);
                    Log.e("name", j+"-----"+user.getUserName());
                    if (!user.getUserName().equals(sharedPref.getString("userName", null))) {
                        //clear shared
                        getSharedPreferences("myPrefs", 0).edit().clear().apply();
                    }
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("firstLogin", 1);
                editor.putString("roleID",user.getROLID());
                editor.putString("userID",user.getUSERID());
                editor.putString("userName",user.getUserName());
                editor.apply();
                //another user to login with darshaboelnasr, but don't mess with anything concerning this user
                intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
                finish();
            }else if(user==null){
                Toast.makeText(LoginActivity.this,R.string.server_error,Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(LoginActivity.this,R.string.check_input,Toast.LENGTH_SHORT).show();
            }
        }
    }
}