package com.ikdynamics.elmalnewseditorial;



import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.content.Intent;
import android.util.Log;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Helpers.Constants;
import Helpers.ServiceHandler;
import Models.ListServiceResponse;
import Models.TopicsModel;


public class MyService extends Service {



    /** interface for clients that bind */
    private IBinder mBinder;

    /** indicates whether onRebind should be used */
    private boolean mAllowRebind;

    private ArrayList<TopicsModel> list;

    /** Called when the service is being created. */
    @Override
    public void onCreate() {

    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        String toBeSentList = sharedPref.getString("sentList",null);
        Gson gson = new Gson();

        if(toBeSentList!=null){
            list = new ArrayList<>(Arrays.asList(gson.fromJson(toBeSentList, TopicsModel[].class)));
            Log.e("tagService", list + "");
            if(list.size()!=0) {
                new sendDataList().execute(Constants.url + "AddArticleList");
            }
        }


        // Let it continue running until it is stopped.

        return START_STICKY;

    }

    /** A client is binding to the service with bindService() */
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("tag","onBind");
        return mBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("tag","onUnbind");
        return mAllowRebind;
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {
        Log.e("tag","onRebind");
    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
    Log.e("Destroyed","Destroyed");
    }

    //post object to server async task
    private class sendDataList extends AsyncTask<String, Void, ListServiceResponse> {

        @Override
        protected ListServiceResponse doInBackground(String... params) {

            ListServiceResponse result;
            Gson gson = new Gson();
            ServiceHandler sh = new ServiceHandler();
                String response = sh.makeServiceCall(params[0],gson.toJson(list));
                int status = sh.status;
                if (status == 200 && response != null) {
                    result = gson.fromJson(response, ListServiceResponse.class);
                    Log.e("user", response);

                    return result;
                }
                else {
                    return null;
            }
        }



        @Override
        protected void onPostExecute(ListServiceResponse result) {
            super.onPostExecute(result);


            if (result != null && result.getFailedStoriesTitles() == null ) {
                list.clear();
                SharedPreferences sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                Gson gson = new Gson();
                editor.putString("sentList", gson.toJson(list));
                editor.apply();
                    MainActivity.fireUi();
                Log.e("list",gson.toJson(list)+"");
            }
        }
    }
}